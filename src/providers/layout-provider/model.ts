export interface LayoutProviderInterface {
  layout?: LAYOUT_TYPES;
}

export enum LAYOUT_TYPES {
  MAIN = "main-layout",
  AUTH = "auth-layout",
}
