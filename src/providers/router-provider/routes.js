import HomePage from '@/pages/home-page';
import NotFound from '@pages/not-found-page';
import paths from './utils/paths';

const routes = [
  // rewards
  {
    path: paths.HOME,
    component: HomePage,
    isPrivate: true,
    exact: true,
  },

  // general routes
  {
    path: paths.NOT_FOUND,
    component: NotFound,
    isPrivate: false,
    exact: false,
  },
];

export default routes;
