const pathes = {
  // ROUTES
  HOME: "/",
  REGISTER_USER: "/user/register",

  // Auth Pages
  LOGIN: "/login",

  // general
  NOT_FOUND: "*",
};

export default pathes;
