import { Store, Dispatch, Action } from "redux";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const crashReporterMiddleWare = (store: Store) => (next: Dispatch) => (
  action: Action
) => {
  try {
    return next(action);
  } catch (err) {
    throw err;
  }
};

export default crashReporterMiddleWare;
