export interface AppProviderInterface {
  children: React.ReactChild[];
}
