import React from 'react';

const MainLayout = ({ children }) => {
  return <div>main layout: {children}</div>;
};

export default MainLayout;
